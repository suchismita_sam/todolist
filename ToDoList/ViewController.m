//
//  ViewController.m
//  ToDoList
//
//  Created by Click Labs134 on 9/30/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

NSMutableArray *enteredToDoList;

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) IBOutlet UIButton *addingListNotes;
@property (strong, nonatomic) IBOutlet UILabel *toDoListLabel;
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UIButton *addButton;
@property (strong, nonatomic) IBOutlet UITableView *list;

@end

@implementation ViewController
@synthesize backgroundImage;
@synthesize addingListNotes;
@synthesize toDoListLabel;
@synthesize textField;
@synthesize addButton;
@synthesize list;

- (void)viewDidLoad {
    [super viewDidLoad];
    enteredToDoList=[[NSMutableArray alloc]init];
    
    [textField setEnabled:NO];
    [addButton setEnabled:NO];
    textField.hidden = YES ;
    addButton.hidden=YES;
    list.hidden=YES;
    
    [addingListNotes addTarget:self action:@selector(addingList:) forControlEvents:UIControlEventTouchUpInside];
    [addButton addTarget:self action:@selector(add:) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) add:(UIButton *)sender
{
    [enteredToDoList addObject:textField.text];
    [list reloadData];
    list.hidden=NO;
}


-(void) addingList:(UIButton *)sender
{
    [textField setEnabled:YES];
    [addButton setEnabled:YES];
    textField.hidden = NO ;
    addButton.hidden=NO;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return enteredToDoList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellReuse"];
    cell.textLabel.text=enteredToDoList[indexPath.row];
    cell.textColor=[UIColor blueColor];
    return cell;
}


-(void)editingChanged:(UITextField *)textField
{
    //if text field is empty, disable the button
    addButton.enabled = textField.text.length > 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
