//
//  main.m
//  ToDoList
//
//  Created by Click Labs134 on 9/30/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
